/*
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene extends Phaser.Scene {
  /*********************************************************************************************************************** */
  constructor() {
    super();
  }

  /*********************************************************************************************************************** */
  preload() {
    this.load.spritesheet("bubble", "https://cdn.glitch.com/b4867de1-6fde-4b66-8d3b-372f0bbac62f%2Fbubbles.png?v=1618320068622", {
      frameWidth: 16,
      frameHeight: 16
    });
    this.load.image("tub", "https://cdn.glitch.com/b4867de1-6fde-4b66-8d3b-372f0bbac62f%2F32c872ebda4cdb1d28d70a9cbaf75131.jpg?v=1618364288240");
    this.load.image("wall_LR", "https://cdn.glitch.com/b4867de1-6fde-4b66-8d3b-372f0bbac62f%2Fdownload%20(2).png?v=1618352699141");
    this.load.image("wall_UD", "https://cdn.glitch.com/b4867de1-6fde-4b66-8d3b-372f0bbac62f%2Fdownload%20(1).png?v=1618352695153");
  }

  /*********************************************************************************************************************** */
  create() {
    //   https://www.loom.com/share/76e7f7f947df4c7ba6b0a752423ba50a
  }
  /*
  bubble 跟上左右三面牆壁碰撞的時候請呼叫這個函式
  */
  collideWithWalls(bubble, wall){
    bubble.disableBody(true, true);
  }

  /*********************************************************************************************************************** */
  update() {
    if (this.cursors.space.isDown) {
      for(let i=0; i<2*Math.PI; i+=0.1){
        let vx=100*Math.cos(i)+(Math.random()*80-40);
        let vy=100*Math.sin(i)+(Math.random()*80-40);
        let bubble=new Bubble(this, 360, 270);
        this.bubbles.push(bubble);
        //讓泡泡飛，並請讓其播放動畫
      }
    }
  }
}
